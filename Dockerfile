FROM php:8.1-apache

RUN apt-get update
RUN apt-get -y install unzip

COPY matomo.zip /usr/local/matomo.zip
RUN unzip /usr/local/matomo.zip -d /var/www/html

WORKDIR /var/www/html
COPY index.php .

EXPOSE 80
EXPOSE 443

CMD /usr/sbin/apache2ctl -D FOREGROUND